from helpers import *

# Network class
#
# This class represents a generalized network to be used for functional approximation.
#
# Given
# - x : a set of inputs of dim N,D_IN,
# - y : a sets of targets outputs yt of dim N,D_OUT
# - a network representation build a succession of graph and transfer functions (embedding their
#   derivatives, see functions in helpers.py)
#
# This class can be used to train the network, i.e. update the weights, so that the error between
# the output of the network and the targets outputs is reduced
#
#
# Example : simple perceptron
#
#       from allumette import *
#
#       # With Network class helpers and automatic weight management
#       d_in = 2
#       d_h1 = 4
#       d_h2 = 20
#       d_out = 1
#       N = 200000
#       x = tensor.uniform(N, d_in,[-2,2])
#
#       yt = (2.36*x[:,0]+x[:,1]+2).view(-1,1)
#
#
#       net = Network(D_IN=d_in,D_OUT=d_out)
#       net \
#           .fcl(d_h1) \
#           .identity() \
#           .fcl(d_h2) \
#           .identity() \
#           .fcl() \
#           .identity() \
#
# Which can be learned by iteratively running
#       # runs the forward and backward path and stores the intermediary results
#       net(x,yt)
#       # Applies the weight update
#       net._dw(lr=lr)
#
class Network:
    # Constructor : takes to mandatory input D_IN and D_OUT,
    # Example of network creation : net = Network(D_IN=10,D_OUT=2)
    def __init__(self,**kwargs):
        if('D_IN' not in kwargs or 'D_OUT' not in kwargs):
            raise ValueError('Missing arguments, both D_IN and D_OUT have to be provided when creating a Network!')
        if(type(kwargs['D_IN']) != int or type(kwargs['D_OUT']) != int):
            raise ValueError('Type error, inputs must be integer!')
        # Input dimension of the selfwork
        self.D_IN = kwargs['D_IN']
        # Output dimension of the selfwork
        self.D_OUT = kwargs['D_OUT']
        # function of the forward path
        self.graph = []
        # functions of the backward pass
        self.dgraph = []
        # Network weights
        self.weights = []
        # Network biases
        self.biases = []
        # Weights initializer
        self.weights_initializer = xavier_initializer
        # The weight derivatives
        self.dweights = []
        self.dweights_rmsprops = []
        self.dweights_momentums = []
        # The biases derivatives
        self.dbiases = []
        self.dbiases_rmsprops = []
        self.dbiases_momentums = []
        # The memory of the forward pass, to calculate gradients w.r.t. parameters
        self.memory = []
        # The memory of the backward pass, to calculate weights update
        self.dmemory = []
        # Whether the selfwork has been closed, i.e. last layer connected to output
        self._closed = False
        # The loss function, set with .loss
        self._loss = None
        # The momentum term
        self.momentum = 0.9
        self.rmsprop = 0.999

    # network sanity check decorator
    def _OnlyIfNetworkIsClosed(f):
        def cb( self, *args, **kwargs ) :
            if(not self._closed):
                raise ValueError('Network is already closed you can not execute this function')
            return f( self, *args, **kwargs )
        return cb

    # network sanity check decorator
    def _OnlyIfNetworkIsOpen(f):
        def cb( self, *args, **kwargs ) :
            if(self._closed):
                raise ValueError('Network is already closed you can not execute this function')
            return f( self, *args, **kwargs )
        return cb

    # network sanity check decorator
    def _OnlyIfLossIsSet(f):
        def cb( self, *args, **kwargs ) :
            if(getattr(self, '_loss') is None):
                raise ValueError("No internal _loss function to use set it with the loss method")
            return f( self, *args, **kwargs )
        return cb

    # __call__ or () method
    #
    # Main method to train a Network on a minibatch.
    # This function does the following :
    #     1) runs the forward path and stores important quantities
    #     2) runs the backward path and stores important quanitities
    #     3) uses step 1) and 2) to calculate the weight update
    #     4) apply the weights update.
    # If you don't want to apply the weights update
    # You can run it with the optional parameter update=False
    # Input:
    #     x   : minibatch [N, D_IN]
    #     yt  : target output [N, D_OUT]
    @_OnlyIfLossIsSet
    @_OnlyIfNetworkIsClosed
    def __call__(self,x,yt,update=True):
        # print('Training on minibtach of size {}'.format(list(x.shape))) # DEBUG
        y = self.forward(x)
        self.backward(self._loss.d(y,yt))
        return self._loss(y,yt)/yt.shape[0]

    # The forward path public method
    def forward(self,x,yt=None):
        # print("forward path") # DEBUG
        y = self._forward(x) #+tensor.float(x.shape).normal_(0,0.1))
        # print("end") # DEBUG
        if(yt is not None):
            return self._loss(y,yt)
        else:
            return y

    # The backward path public method
    def backward(self,dloss):
        # print("backward path") # DEBUG
        y = self._backward(dloss,len(self.graph)-1)
        # print("end") # DEBUG

    # Internal recursive function for the forward path
    # Stores intermediary results and returns the network output
    def _forward(self,x,i=0):
        # print(x.shape) # DEBUG
        # import ipdb; ipdb.set_trace()
        if(i == len(self.graph)):
            return x
        self.memory[i][0] = x
        if(self.memory[i][1] == 0):
            return self._forward(self.graph[i](x,self.weights[int(i/2)],self.biases[int(i/2)]),i+1)
        else:
            return self._forward(self.graph[i](x),i+1)

    # Internal recursive function for the backward path
    # Stores intermediary results
    def _backward(self,x,i):
        # print(x.shape,self.dmemory[i][1]) # DEBUG
        self.dmemory[i][0] = x
        if(i == 1):
            return x
        if(self.dmemory[i][1] == 0):
            return self._backward(self.dgraph[i].d(x,self.weights[int(i/2)]),i-1)
        else:
            return self._backward(x*self.dgraph[i](self.memory[i][0]),i-1)

    # Internal recursive function for the weight update
    # Uses intermediary results of forward and backward path and updates the weights if lr is set
    def _dw(self,i=0,lr=None):
        # internal method used to calculate the weights derivatives
        if(i == len(self.weights)):
            return
        x = self.memory[2*i][0]
        dy = self.dmemory[2*i+1][0]
        dw = lambda x,dy : x.t().mm(dy)
        self.dweights[i] = dw(x,dy)
        self.dbiases[i] = dy.sum(0)
        self.dweights_momentums[i] = self.momentum*(self.dweights_momentums[i])+(1-self.momentum)*self.dweights[i]
        self.dweights_rmsprops[i] = self.rmsprop*self.dweights_rmsprops[i]+(1-self.rmsprop)*self.dweights[i]**2
        self.dbiases_momentums[i] = self.momentum*(self.dbiases_momentums[i])+(1-self.momentum)*self.dbiases[i]
        self.dbiases_rmsprops[i] = self.rmsprop*self.dbiases_rmsprops[i]+(1-self.rmsprop)*self.dbiases[i]**2
        #self.dweights[i] = -lr*dw(x,dy)
        if(lr is not None):
            self.weights[i] -= lr*self.dweights_momentums[i]/(self.dweights_rmsprops[i].sqrt()+1e-8)
            self.biases[i] -= lr*self.dbiases_momentums[i]/(self.dbiases_rmsprops[i].sqrt()+1e-8)
        self._dw(i+1,lr)

    # fcl : fullyConnectedLayer
    #
    # Takes on optional input - D_OUT - corresponding to the desired number of outputs
    #
    # To close a network one calls fcl without input. In that case D_OUT is set to the
    # output dim of the network (set at the creation of the network)
    #
    #
    #       net = Network(D_IN=d_in,D_OUT=d_out)
    #       net \
    #           .fcl(d_h1) \
    #           .identity() \
    #           .fcl(d_h2) \
    #           .identity() \
    #           .fcl() \        # HERE THE NETWORK IS CLOSED. NO MORE LAYER CAN BE ADDED
    #           .identity() \
    @_OnlyIfNetworkIsOpen
    def fcl(self,D_OUT=-1,dropout=0.0):
        if(D_OUT == -1):
            self._closed = True
            D_OUT = self.D_OUT
        if(len(self.weights) == 0):
            D_IN = self.D_IN
        else:
            D_IN = self.weights[-1].shape[1]
        #self.weights.append(tensor.float(D_IN,D_OUT).normal_(0,1e-1))
        self.weights.append(self.weights_initializer(D_IN,D_OUT))
        self.biases.append(tensor.zero(1,D_OUT))
        self.dweights.append(tensor.zero(D_IN,D_OUT))
        self.dweights_momentums.append(tensor.zero(D_IN,D_OUT))
        self.dweights_rmsprops.append(tensor.zero(D_IN,D_OUT))
        self.dbiases.append(tensor.zero(1,D_OUT))
        self.dbiases_momentums.append(tensor.zero(1,D_OUT))
        self.dbiases_rmsprops.append(tensor.zero(1,D_OUT))
        self.graph.append(fcl(dropout=dropout))
        self.dgraph.append(self.graph[-1])
        self.memory.append([tensor.float(),0])
        self.dmemory.append([tensor.float(),0])
        return self

    # func: methods used to add a transfer function to the tail of the network
    #
    # Takes a mandatory input: the function to be added, functions from helpers.py
    #
    def func(self,f):
        #self.graph[-1] = f(self.graph[-1])
        self.graph.append(f())
        self.dgraph.append(f.d())
        self.memory.append([tensor.float(),1])
        self.dmemory.append([tensor.float(),1])
        return self

    # Adds a relu transfer function to the tail of the network, see helpers for function implementation
    def relu(self):
        return self.func(relu())

    # Adds a identity transfer function to the tail of the network, see helpers for function implementation
    def identity(self):
        return self.func(identity())

    # Adds a tanh transfer function to the tail of the network, see helpers for function implementation
    def tanh(self):
        return self.func(tanh())

    # Adds a oneify transfer function (used for debug purposes) to the tail of the network, see helpers for function implementation
    def oneify(self):
        return self.func(oneify())

    # Assigns an internal loss to the network, see helpers for loss implementation example
    def loss(self,f):
        self._loss = f()

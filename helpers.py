from torch import FloatTensor as ft
from torch import LongTensor as lt

# Similar to oneHot tensorflow function
# Transforms a D label vector of length N: [0,1,0,0,0,1]
# Into a vector of dimension [NxD]:
# [1,0]
# [0,1]
# [0,0]
# [0,0]
# [0,0]
# [0,1]
def oneHot(label):
    N = label.shape[0]
    labels_one_hot_0 = tensor.float(N,2).zero_()
    labels_one_hot_1 = tensor.float(N,2).zero_()
    labels_one_hot_0.scatter_(1, (label==0).long(), 0)
    labels_one_hot_1.scatter_(1, (label==0).long(), 1)
    return labels_one_hot_0+labels_one_hot_1

def oneHotnD(labels,D=2,label=0):
    #import ipdb; ipdb.set_trace()
    N = labels.shape[0]
    if(label == N):
        return 0

    labels_one_hot_n = tensor.float(N,D).zero_()
    labels_one_hot_n.scatter_(1, (labels==0).long(), label)
    return labels_one_hot_n+oneHotnD(labels,D,label+1)


# The tensor class is a helper for the creation of tensors,
# It can be used to generate torch.FloatTensor (using float) and torch.LongTensor(using long)
# and to initialize 2D tensors, three methods are currently available :
#
# - zero(N_row,N_col)
# - uniform(N_row,N_col,range=[0,1])
# - gaussian(N_row,N_col,mu=0,sigma=1)
class tensor:
    floatPrecision = 1e10
    def float(*args,**kwargs):
        return ft(*args,**kwargs)
    def long(*args,**kwargs):
        return lt(*args,**kwargs)
    def zero(nx,ny):
        return ft(nx,ny).random_(1)
    def uniform(nx,ny,range=[0,1]):
        baseline = range[0]
        range[0] -= baseline
        range[1] -= baseline
        _min = int(range[0]*tensor.floatPrecision)
        _max = int(range[1]*tensor.floatPrecision)
        return baseline+ft(nx,ny).random_(_min,to=_max)/tensor.floatPrecision
    def gaussian(nx,ny,mu=0,sigma=1):
        return ft(nx,ny).normal_(mu,sigma)


# Layers
#
# Layer classes needs to implement 2 methods
# __call__ : a method that executes the calculation corresponding to the forward path
# d : a method that returns a function representing the derivative of the __call__ method
class layer:
    def __call__(self,x,w,b):
        raise NotImplementedError("Error layer class needs to implement a __call__ function with signature (self,x,w,b)")
    def d(self):
        raise NotImplementedError("Error layer class needs to implement a derivative function called d that returns a function calculating the derivative")

# Fully Connected layer helper class
class fcl:
    def __init__(self,dropout=0.5):
        if(not (dropout == 0.5 or dropout == 0.0)):
            print(dropout)
            raise ValueError("Error only 0.5 is currently allowed for dropout")
        self.dropout = int(dropout*2+1)
    def __call__(self,x,w,b):
        self.d_mask = 1-tensor.float(w.shape).random_(self.dropout)
        return x.mm(w*self.d_mask)+b
    def d(self,x,w):
        return x.mm((w*self.d_mask).t())


# Functions helper class
#
# Function class needs to implement 2 methods
# __call__ : a method that returns a function corresponding to the calculation of the forward path
# d : a method that returns a function representing the derivative of the __call__ method

# Identity helper class
class identity:
    def __call__(self):
        return lambda x: x
    def d(self):
        return lambda x: 1

# Tanh function helper class
class tanh:
    def __call__(self):
        return lambda x: x.tanh()
    def d(self):
        return lambda x: 4 * (x.exp() + x.mul(-1).exp()).pow(-2)

# Relu function helper class
class relu:
    def __call__(self):
        return lambda x: x.clamp(min=0)
    def d(self):
        return lambda x: (x > 0).float()

# Tanh function helper class
class mse:
    def __call__(self,v,t):
        return (v - t).pow(2).sum()
    def d(self,v,t):
        return 2*(v-t)

xavier_initializer = lambda D_IN,D_OUT: tensor.float(D_IN,D_OUT).normal_(0,1.0/tensor.float([D_IN+D_OUT]).sqrt()[0])

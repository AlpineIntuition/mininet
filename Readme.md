## Allumette: sometimes you don't need a full torch

Allumette is a minimalistic deep learning framework. 

Consisting of less than 400 lines of codes, its functional nature allows it to be extended in a straight forward manner allowing easy prototyping.
Performance tests are planned for the future, but not the main goal.

- It features : 
  - xavier_initialization
  - momentum sgd
  - transfer function : tanh, relu, identity
  - layers : fully connected layer  


## Tests

This repo comes with the library (allumette.py and helpers.py) and two examples (test.py, perceptron.py).

- test.py runs the following test :
> training and a test set of 1, 000 points sampled uniformly in [0, 1]^2 each with a
> label 0 if outside the disk of radius 1/√2π and 1 inside
> 
> network with two input units, two output units, three hidden layers of 25 units

- perceptron.py sanity check with linear activation function and affine function fit.

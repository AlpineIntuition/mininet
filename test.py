from allumette import *
import numpy, scipy.io
import torch
#
# NETWORK DEFINITION
# ------------------
#

# CONSTANT
d_in = 5    # Input dimension
d_h1 = 20   # Hidden dimension 1
d_h2 = 500   # Hidden dimension 2
d_h3 = 20  # Hidden dimension 3
d_out = 32   # Output dimension


# TRAINING AND TESTING SET CREATION
# PI = 3.141592653589793
# N = 1000    # Number of samples
# x = tensor.uniform(N,d_in,[0,1])
# label = (x.norm(2,dim=1) <= 1.0/(tensor.float([2*PI]).sqrt()[0])).float().view(-1,1)
# x_test = tensor.uniform(N,d_in,[0,1])
# label_test = (x_test.norm(2,dim=1) <= 1.0/(tensor.float([2*PI]).sqrt()[0])).float().view(-1,1)
# yt = oneHot(label)
# yt_test = oneHot(label_test)


data = scipy.io.loadmat('/home/efx/Downloads/data.mat')

x = tensor.float(data['Y']).t()
yt = tensor.float(data['X']).t()


x_test = x[int(x.shape[0]*0.8):,:]
yt_test = yt[int(yt.shape[0]*0.8):,:]


x = x[1:int(x.shape[0]*0.8)-1:,:]
yt = yt[1:int(yt.shape[0]*0.8)-1:,:]


electrode_1_activities_test = yt_test[:,0:15].sum(1)
electrode_2_activities_test = yt_test[:,15:].sum(1)

electrode_1_activities = yt[:,0:15].sum(1)
electrode_2_activities = yt[:,15:].sum(1)

reformat_vector = lambda electrodes,act_elec_1,act_elec_2: torch.cat([electrodes,torch.cat([act_elec_1.view(-1,1),act_elec_2.view(-1,1)],1)],1)

yt = reformat_vector((yt > 0).float(),electrode_1_activities,electrode_2_activities)
yt_test = reformat_vector((yt_test > 0).float(),electrode_1_activities_test,electrode_2_activities_test)





N = x.shape[0]    # Number of samples





# NETWORK BUILDING
net = Network(D_IN=d_in,D_OUT=d_out)
net \
    .fcl(d_h1) \
    .relu() \
    .fcl(d_h2,dropout=0.5) \
    .relu() \
    .fcl(d_h3) \
    .tanh() \
    .fcl() \
    .identity() \

net.loss(mse) # Set the loss to mse
net.momentum = 0.5 # Set the momentum to 0.8
net.rmsprop = 0.9

#
# LEARNING
# ------------------
#

miniBatch = 50               # Size of each miniBatch
epochNumbers = 2000         # Number of epoch to run
miniBatchPerEpoch = 10      # Number of minibatch to run per epoch
base_lr = 1e-4              # Base learning rate

pred = lambda y: oneHot(1-y.max(1)[1].view(-1,1).float())
error_percentage = lambda y,yt: float(((yt-y).abs()>0).sum())/yt.shape[0]*50
def error_percentage(y,yt):
    import ipdb; ipdb.set_trace()
    return float(((yt-y).abs()>0).sum())/yt.shape[0]*50

rnd = lambda x: round(x*100)/100.0

epoch = 0
errorTest = 0
while epoch < epochNumbers:
    n = 0
    starts = tensor.long(miniBatchPerEpoch+1).random_(N-miniBatch)
    lr = (1.0**epoch)*base_lr
    loss = 0
    t = 0
    error = 0
    while True:
        start = starts[n]
        end = start+miniBatch
        if(n >= miniBatchPerEpoch):
            break
        t = pred(net.forward(x[start:end,:]))
        net(x[start:end,:],yt[start:end,:])
        net._dw(lr=lr)
        n = n+1
    epoch += 1
    import ipdb; ipdb.set_trace()
    p = (net.forward(x[start:end,:],yt[start:end,:]))/(end-start)
    p_test = (net.forward(x_test,yt_test))/yt_test.shape[0]
    error = p
    errorTest = p_test
    print("Epoch: {}/{}, lr:{}".format(epoch,epochNumbers,lr))
    print("error_train: {}, error_test: {}%".format(rnd(error),rnd(errorTest)))

print("error_train: {}, error_test: {}%".format(rnd(error),rnd(errorTest)))

#net.backward(y,yt)
#net(x,yt).updateWeight()

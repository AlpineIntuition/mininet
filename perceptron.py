from allumette import *

# With Network class helpers and automatic weight management
d_in = 2
d_h1 = 4
d_h2 = 20
d_out = 1
N = 200000
x = tensor.uniform(N, d_in,[-2,2])

#yt = 2*((x.norm(2,dim=1) < 1.5).float().view(-1,1)-0.5)
#yt = (x[:,0] < x[:,1]).float().view(-1,1)
yt = (2.36*x[:,0]+x[:,1]+2).view(-1,1)


#TODO : no two fcl can
net = Network(D_IN=d_in,D_OUT=d_out)
net \
    .fcl(d_h1) \
    .identity() \
    .fcl(d_h2) \
    .identity() \
    .fcl() \
    .identity() \

net.loss(mse)


miniBatch = 200
epoch = 0

epochNumbers = 100
miniBatchPerEpoch = 200
base_lr = 1e-6
while epoch < epochNumbers:
    n = 0
    starts = tensor.long(miniBatchPerEpoch+1).random_(N-miniBatch)
    lr = (0.999**epoch)*base_lr
    while True:
        start = starts[n]
        end = start+200
        if(n >= miniBatchPerEpoch):
            break
        loss = net(x[start:end,:],yt[start:end,:])
        net._dw(lr=lr)
        #print(loss)
        n = n+1
    epoch += 1
    t = 2*((net.forward(x[start:end,:])>0).float()-0.5)
    error = float(((yt[start:end,:]-t).abs()>0).sum())/miniBatch*100
    #print("Epoch: {}, lr:{}, loss: {}, error: {}%".format(epoch+1,lr,loss,error))
    print("Epoch: {}, lr:{}, loss: {}".format(epoch+1,lr,loss))

net.forward(x[0:10])-(x[0:10,0]*2.36+x[0:10,1]).view(-1,1)



#net.backward(y,yt)
#net(x,yt).updateWeight()
